from typing import List
from django.apps import AppConfig


class MyAppConfig(AppConfig):
    def ready(self):
        from .providers import ModelProvider, ServiceProvider

        ModelProvider()
        ServiceProvider()
