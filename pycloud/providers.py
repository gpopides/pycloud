from typing import List


class ModelProvider:
    def __new__(cls, *args, **kwargs):
        import api.models
        import auth.models

        _cls = super().__new__(cls, *args, **kwargs)
        _cls.models = {
            "auth": {"user": auth.models.PycloudUser},
            "api": {"file": api.models.UserFile, "directory": api.models.UserDirectory},
        }
        return _cls

    def inject(self, namespace: str, model_names: List[str]):
        def inner(service):
            models_of_namespace = self.models[namespace]
            _models = {}

            for model in model_names:
                model_to_inject = models_of_namespace.get(model)

                if model_to_inject:
                    setattr(service, f"{model}_model", model_to_inject)

            return service

        return inner


class ServiceProvider:
    def __new__(cls, *args, **kwargs):
        from api.services.file_service import FileService
        from auth.services.authentication_service import AuthenticationService
        import auth.services

        _cls = super().__new__(cls, *args, **kwargs)
        _cls.services = {
            "auth": {"authentication": AuthenticationService()},
            "api": {"file": FileService()},
        }
        return _cls

    def inject(self, namespace: str, service_name: str):
        def inner(view, *args, **kwargs):
            service_to_inject = self.services.get(namespace).get(service_name)
            if service_to_inject:
                setattr(view, "service", service_to_inject)
            return view

        return inner
