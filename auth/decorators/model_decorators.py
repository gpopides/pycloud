import functools
from django.db.utils import IntegrityError
from rest_framework.response import Response
from rest_framework import status


def catch_integrity_error(*, model=None, action: str):
    def decorator(view_method):
        """
        Tries to execute a method that may result
        to a IntegrityError.  If that happens,
        returns that the item exists, with status
        code 400.
        """

        @functools.wraps(view_method)
        def inner_method(*args, **kwargs):
            try:
                return view_method(*args, **kwargs)
            except IntegrityError as e:
                return Response(
                    {
                        "success": False,
                        "entity": model._meta.model_name,
                        "action": action,
                        "message": "alreadyExists",
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )

        return inner_method

    return decorator
