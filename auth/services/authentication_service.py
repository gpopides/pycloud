from typing import Dict
from datetime import timedelta
from django.utils import timezone
from rest_framework import exceptions
import redis
from ..models import PycloudUser
from ..utils import create_new_token
from ..exceptions.register_fail import RegisterFailException
from ..serializers.auth import (
    LoginSuccessResponseSerializer,
    LoginErrorResponseSerializer,
)
from api.utils import get_dotenv_values

REDIS_HOST, REDIS_PORT = get_dotenv_values(variables=("REDIS_HOST", "REDIS_PORT"))

DATE_FORMAT = "%Y-%m-%d %H:%M"


class AuthenticationService:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = PycloudUser
        self.redis_connection = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)

    def register_user(self, user_data: Dict):
        new_user = PycloudUser.objects.create_user(**user_data)

        if not new_user:
            raise RegisterFailException("registration failed")

        return {"success": True, "username": new_user.username}

    def get_user_token(self, user_credentials) -> LoginSuccessResponseSerializer:
        user = self.model.objects.get(username=user_credentials["username"])

        if not user.check_password(user_credentials["password"]):
            raise exceptions.AuthenticationFailed("check_email_password")

        new_token = create_new_token(user.username)

        if self.redis_connection.set(new_token, user.username):  # add to redis worked
            self.redis_connection.expire(
                new_token, timedelta(seconds=60 * 60 * 8)
            )  # 1H

            if user.last_login:
                last_login = user.last_login.strftime(DATE_FORMAT)
            else:
                last_login = timezone.now().strftime(DATE_FORMAT)

            serializer = LoginSuccessResponseSerializer(
                {
                    "success": True,
                    "token": new_token,
                    "username": user.username,
                    "user_id": user.id,
                }
            )
            # update the last login
            user.last_login = timezone.now()
            user.save()

        else:
            serializer = LoginErrorResponseSerializer(
                {"success": "False", "error": "token_not_created"}
            )

        return serializer.data
