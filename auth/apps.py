from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = "auth"
    label = "pycloud_auth"
