from rest_framework import serializers


class RegisterRequestSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=50)
    email = serializers.EmailField(max_length=50)
    password = serializers.CharField(max_length=100)


class LoginRequestSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=50)
    password = serializers.CharField(max_length=100)


class LoginSuccessResponseSerializer(serializers.Serializer):
    success = serializers.BooleanField()
    token = serializers.CharField(max_length=50)
    username = serializers.CharField(max_length=20)
    user_id = serializers.IntegerField()


class LoginErrorResponseSerializer(serializers.Serializer):
    success = serializers.BooleanField()
    error = serializers.CharField(max_length=50)
