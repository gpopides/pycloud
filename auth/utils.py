import hashlib


def create_new_token(username: str) -> str:
    return hashlib.sha256(username.encode("utf-8")).hexdigest()


if __name__ == "__main__":
    pass
