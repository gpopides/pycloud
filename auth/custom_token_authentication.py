import json
from django.contrib.auth import get_user
from rest_framework import authentication
from rest_framework import exceptions
from rest_framework import permissions
import redis
from .models import PycloudUser
from api.utils import get_dotenv_values

REDIS_HOST, REDIS_PORT = get_dotenv_values(variables=("REDIS_HOST", "REDIS_PORT"))


class CustomTokenAuthentication(authentication.BaseAuthentication):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.redis_connection = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)

    def authenticate(self, request):
        auth_header = request.META.get("HTTP_AUTHORIZATION")

        if not auth_header:
            raise exceptions.AuthenticationFailed('no_token_sent')

        token_type, request_token = auth_header.split()

        if token_type != "Bearer" or not request_token:
            raise exceptions.AuthenticationFailed("wrong_token")

        token_ttl = self.redis_connection.ttl(request_token)

        if token_ttl > 0:
            try:
                username_from_token = self.redis_connection.get(request_token)
                user = PycloudUser.objects.get(
                    username=username_from_token.decode("utf-8")
                )
            except PycloudUser.DoesNotExist:
                raise exceptions.AuthenticationFailed("user_does_not_exist")
        else:
            raise exceptions.AuthenticationFailed("token_expired")

        return user, None


class PycloudAuthenticationPermission(permissions.BasePermission):
    def __inti__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def has_permission(self, request, view):
        if request.method == "POST":
            return True
        else:
            return False
