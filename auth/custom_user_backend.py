from django.contrib.auth.hashers import check_password
from rest_framework import exceptions
from rest_framework.response import Response
import redis
from .models import PycloudUser
from api.utils import get_dotenv_values


REDIS_HOST, REDIS_PORT = get_dotenv_values(variables=("REDIS_HOST", "REDIS_PORT"))


class CustomUserBackend:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.redis_connection = redis.Redis(host=REDIS_HOST, port=REDIS_PORT)

    def authenticate(self, request):
        """
        Authentication for PycloudUser
        """
        token_type, request_token = request.META.get("HTTP_AUTHORIZATION").split()

        if token_type != "Bearer" or not request_token:
            raise exceptions.AuthenticationFailed("no_token_sent")

        token_ttl = self.redis_connection.ttl(request_token)
        username_from_token = self.redis_connection.get(request_token)

        if token_ttl > 0:
            try:
                user = PycloudUser.objects.get(
                    username=username_from_token.decode("utf-8")
                )
            except PycloudUser.DoesNotExist:
                raise exceptions.AuthenticationFailed("user_does_not_exist")
        else:
            pass
            # raise exceptions.AuthenticationFailed('token_expired')
            # return Response("token_expire")
        return user, None

    def get_user(self, user_id):
        """
        Gets PycloudUser
        """
        try:
            user = PycloudUser.objects.get(id=user_id)
        except PycloudUser.DoesNotExist:
            raise exceptions.AuthenticationFailed("wtf")

        return user
