from django.contrib.auth import authenticate, logout
from rest_framework.response import Response
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from rest_framework import permissions
from ..serializers.auth import RegisterRequestSerializer, LoginRequestSerializer
from ..services.authentication_service import AuthenticationService
from ..custom_token_authentication import PycloudAuthenticationPermission
from ..decorators.model_decorators import catch_integrity_error
from ..models import PycloudUser


auth_service = AuthenticationService()


@api_view(["POST"])
@permission_classes((PycloudAuthenticationPermission,))
def login(request):
    login_serializer = LoginRequestSerializer(data=request.data)

    if login_serializer.is_valid(raise_exception=True):
        login_response = auth_service.get_user_token(login_serializer.validated_data)

    return Response(login_response)


@api_view(["POST"])
@catch_integrity_error(model=PycloudUser, action="create")
def register(request):
    user_creation_result = None
    serializer = RegisterRequestSerializer(data=request.data)

    if serializer.is_valid(raise_exception=True):
        user_creation_result = auth_service.register_user(serializer.validated_data)

    return Response(user_creation_result)
