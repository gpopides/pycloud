from rest_framework import serializers
from ..models import UserFile, UserDirectory


class FileSerializer(serializers.Serializer):
    file_type = serializers.CharField(max_length=10)
    attachment = serializers.FileField()


class FileUploadSerializer(serializers.Serializer):
    files_list = FileSerializer(many=True)
    user_directory_id = serializers.CharField(max_length=100)


class FileUpoadResponseSerializer(serializers.Serializer):
    success = serializers.BooleanField(read_only=True)
    file_ids = serializers.ListField(child=serializers.IntegerField())


class UserFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserFile
        exclude = ("attachment", "user")


class UserDirectorySerializer(serializers.ModelSerializer):
    class Meta:
        model = UserDirectory
        fields = ("path",)
        # exclude = ("user",)


class CreateDirectorySerializer(serializers.Serializer):
    name = serializers.CharField(max_length=20)
    parents = serializers.ListField(child=serializers.CharField(max_length=20))
    user_id = serializers.IntegerField(min_value=1)
