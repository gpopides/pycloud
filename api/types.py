from typing import NewType
from rest_framework.request import Request
from rest_framework.response import Response

DrfRequest = NewType("drf_request", Request)
DrfResponse = NewType("drf_response", Response)
