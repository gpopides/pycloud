from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import viewsets
from rest_framework.parsers import MultiPartParser, FormParser, JSONParser
from auth.custom_token_authentication import CustomTokenAuthentication
from pycloud.providers import ServiceProvider
from ..services.file_service import construct_serializer_file_collection
from ..serializers.file_serializers import (
    FileUploadSerializer, CreateDirectorySerializer,
    FileUpoadResponseSerializer, UserDirectorySerializer
)
from ..decorators.model_decorators import catch_does_not_exist


SP = ServiceProvider()


@SP.inject(namespace="api", service_name="file")
class FilesViewSet(viewsets.ViewSet):
    authentication_classes = [CustomTokenAuthentication]
    parser_classes = (JSONParser, MultiPartParser, FormParser)
    parser_classes.count(5)

    service = None

    @action(detail=False, methods=["GET"])
    def get_files(self, request, *args, **kwargs):
        return Response(self.service.get_file_records(request.user.id))

    # upload files
    @catch_does_not_exist("directory")
    def create(self, request, *args, **kwargs):
        user_id = request.user.id

        if not user_id:
            return Response({"error": "no user available"})

        serializer_data = construct_serializer_file_collection(
            request.data.getlist("files"), request.data.get("directory")
        )

        serializer = FileUploadSerializer(data=serializer_data)
        if serializer.is_valid(raise_exception=True):
            result = self.service.create_file_records(
                user_id, serializer.validated_data
            )

        return Response(FileUpoadResponseSerializer(result).data)

    @action(detail=False, methods=["GET"])
    def directories(self, request, *args, **kwargs):
        result = UserDirectorySerializer(
            self.service.get_user_directories(request.user.id),
            many=True
        ).data
        return Response(result)

    @action(detail=False, methods=["POST"])
    def create_directory(self, request, *args, **kwargs):
        serializer_data = {"user_id": request.user.id, **request.data}
        result = None

        serializer = CreateDirectorySerializer(data=serializer_data)

        if serializer.is_valid(raise_exception=True):
            result = self.service.create_directory_record(
                serializer.validated_data
            )

        return Response(result)
