"""
Service that has methods for with files/directories.
"""
import io
import os.path
from typing import Dict, Iterable, Union, Any
from pathlib import Path
import hashlib
from PIL import Image
from django.conf import settings
from django.core.files import File
from pycloud.providers import ModelProvider

MP = ModelProvider()


@MP.inject("api", ["file", "directory"])
class FileService:
    """
    FileService class.
    """
    file_model = None
    directory_model = None

    def __init__(self):
        self.base_upload_path = settings.MEDIA_ROOT

    def get_file_records(self, user_id: int):
        return self.file_model.objects.filter(user_id=user_id).count()

    def create_file_records(self, user_id: int, serializer_data: Dict):
        """
        Creates UserFile record and adds the file to the storage.
        """
        result = {"file_ids": [], "success": False}

        directory_record = self.directory_model.objects.get(
            id=serializer_data.get("user_directory_id")
        )

        bulk_data = []

        for file_dict in serializer_data["files_list"]:
            hashed_name = _hash_file_name(file_dict["attachment"].name)
            f_type = file_dict["file_type"]

            bulk_data.append(self.file_model(
                name=file_dict["attachment"].name,
                hashed_name=f"{hashed_name}.{f_type}",
                user_id=user_id,
                directory_id=directory_record.id,
                file_type=f_type,
                file=file_dict["attachment"],
                thumbnail=create_thumbnail(file_dict["attachment"])
            ))

        # print(bulk_data[0].directory.path)
        new_objects = self.file_model.objects.bulk_create(bulk_data)

        # result["file_ids"] = new_objects.values('id', 'name')
        result["success"] = True

        return result

    def construct_user_directories_json(self, user_id) -> Iterable:
        """
        Create a response that has a list of object where each object
        has a normal string as a path and an array of strings
        that reprecent the path from ROOT to last NODE.
        """
        directories_list = []
        user_base_directory = Path(
            self.base_upload_path
        ).joinpath(str(user_id))
        directories_collection = self._construct_directories_collection(
            user_base_directory, directories_list
        )

        return directories_collection

    def create_directory_record(self, data) -> Dict[str, Union[str, bool]]:
        """
        Create a directory record and create the directory/ies in the path.
        """
        result: Dict[str, Union[str, bool]] = {}
        user_id = str(data["user_id"])
        parent_directories = data["parents"]
        final_directory_name = data["name"]

        new_directory_path = Path(settings.MEDIA_ROOT).joinpath(
            user_id, *parent_directories, final_directory_name
        )

        thumbnails_path = new_directory_path.joinpath("thumbnails")

        record, _ = self.directory_model.objects.update_or_create(
            path=str(new_directory_path),
            user_id=user_id,
            thumbnail_path=str(thumbnails_path)
        )
        new_directory_path.mkdir(parents=True, exist_ok=True)
        thumbnails_path.mkdir(parents=True, exist_ok=True)

        result["directory_id"] = record.id
        result["created"] = record.id is not None

        return result

    def get_user_directories(self, user_id: int) -> Any:
        """
        Returns the user directory records of the user.
        """
        return self.directory_model.objects.filter(user_id=user_id)

    def _construct_directories_collection(
        self,
        dir_path: Path, directories_collection
    ):
        # each time get a path and iter the path
        # for each item in the path check if its a directory.
        # if it is one, create a new dict item where
        # it holds the full path (without the MEDIA ROOT)
        # and has a normalized path attribute where it holds a list of
        # directory names
        # which essentialy are, the parents of it.
        for child in dir_path.iterdir():
            if child.is_dir():
                directory_without_media_root = str(dir_path).replace(
                    settings.MEDIA_ROOT, ""
                )

                directories_collection.append(
                    {
                        "full_path": directory_without_media_root,
                        "normalized_path": str(
                            directory_without_media_root
                        ).split("/")[1:]
                    }
                )

                self._construct_directories_collection(
                    child,
                    directories_collection
                )
            return directories_collection

    def create_thumbnail(self, image_path: str):
        """
        Create a thumbnail.
        """
        print(image_path)


def _hash_file_name(current_file_name: str) -> str:
    """
    Creates a hash as a file name.
    """
    return hashlib.sha224(str.encode(current_file_name)).hexdigest()


def construct_serializer_file_collection(
    files_collection: Iterable,
    directory_id: str
) -> Dict:
    """
    Factory of the data argument for the FileUploadSerializer.
    """
    files_list = []
    for _file in files_collection:
        files_list.append(
            {
                "file_type": os.path.splitext(
                    _file.name
                )[1].replace(".", ""),
                "attachment": _file,
            }
        )

    return {
        "files_list": files_list,
        "user_directory_id": int(directory_id)
    }

def create_thumbnail(new_file):
    # im = Image.open(new_file)
    # im.resize((100, 100))
    # return File(im)
    return new_file
