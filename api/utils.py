import sys
from typing import Tuple
import functools
from rest_framework.response import Response


def get_dotenv_values(*, variables: Tuple[str]):
    env_dict = {}
    try:
        with open(".env") as env_file:
            env_variables_as_pairs = [
                line.rstrip().split("=") for line in env_file.readlines() if line != ""
            ]

            for pair in env_variables_as_pairs:
                if len(pair) == 2:
                    variable_name, varable_value = pair
                    env_dict[variable_name] = varable_value
        env_variables_list = []

        for variable in variables:
            if env_dict.get(variable) is None:
                raise Exception(f"Env {variable} not exist")
            env_variables_list.append(env_dict.get(variable))

        return env_variables_list
    except FileNotFoundError as e:
        sys.stderr.write(".env not found")
        sys.exit()


# def add_user_id_parameter_to_args(view_method):
#     @functools.wraps(view_method)
#     def wrapper(*args, **kwargs):
#         user_id = kwargs.get('user_id')
#         new_args = (*args, user_id)
#         kwargs.pop('user_id', None)
#         return view_method(*new_args, **kwargs)
#     return wrapper
