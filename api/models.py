from django.db import models
from auth.models import PycloudUser
from django.conf import settings


def upload_path(instance, filename):
    return instance.directory.path


def upload_path_thumbnail(instance, filename):
    return instance.directory.thumbnail_path


class UserDirectory(models.Model):
    class Meta:
        db_table = "user_directories"
        app_label = "api"

    id = models.AutoField(primary_key=True)
    path = models.CharField(max_length=100, null=True, unique=True)
    thumbnail_path = models.CharField(max_length=100, null=True, unique=True)
    user = models.ForeignKey(
        PycloudUser,
        on_delete=models.CASCADE,
        related_name="user_directory",
        related_query_name="user_directory",
    )


class UserFile(models.Model):
    class Meta:
        db_table = "user_files"
        app_label = "api"

    id = models.AutoField(primary_key=True)
    file_type = models.CharField(max_length=10, null=True)
    user = models.ForeignKey(
        PycloudUser,
        on_delete=models.CASCADE,
        related_name="user_file",
        related_query_name="user_file",
    )
    directory = models.ForeignKey(
        UserDirectory,
        on_delete=models.CASCADE,
        related_name="directory",
        related_query_name="directory",
        null=True,
    )
    name = models.CharField(max_length=50, null=True)
    hashed_name = models.CharField(max_length=66, null=True)
    file = models.FileField(upload_to=upload_path, default=None)
    thumbnail = models.ImageField(upload_to=upload_path_thumbnail, blank=True)
