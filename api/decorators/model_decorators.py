from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework import status


"""
Instead of populating the services with
try/catch in case a DB record does not exist
we do it here and use this as a decorator
to clean the code and make it more elegant.
"""


def catch_does_not_exist(entity_that_does_not_exist: str):
    def _catch_does_not_exist(view_method):
        def inner(*args, **kwargs):
            # since the decorator is on a class method
            # we cant pass self arguments
            # so we take the first argument which is the class
            # itself and with the name that was passed as a
            # decorator argument, we get the attribute from the
            # class directly
            try:
                return view_method(*args, **kwargs)
            except ObjectDoesNotExist:
                return Response(
                    {"error": f"{entity_that_does_not_exist} does not exist"},
                    status=status.HTTP_404_NOT_FOUND,
                )

        return inner

    return _catch_does_not_exist
