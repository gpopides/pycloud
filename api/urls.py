from django.urls import path, include
from rest_framework import routers
from .views.file_views import FilesViewSet


router = routers.SimpleRouter(trailing_slash=False)

router.register("fs", FilesViewSet, basename="user_files")

urlpatterns = [
    path("users/<int:user_id>/", include(router.urls)),
]

urlpatterns += router.urls
